#!/bin/bash

sudo apt-get update

sudo ufw allow http
sudo ufw allow https
sudo ufw allow ssh
sudo ufw enable

curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo mkdir -p /var/www/ddnshub
sudo chown $USER /var/www/ddnshub

# Install and configure nginx
sudo apt-get install nginx -y

# Disable default site
echo "server {
        listen 80 default_server;
        listen [::]:80 default_server;

        server_name _;
        return 444;
}" | sudo tee /etc/nginx/sites-enabled/default > /dev/null

# Using "Here document"
# If sudo is not necessary use 'cat > path/to/file <<EOF'
cat << 'EOF' | sudo tee /etc/nginx/conf.d/ddnshub.net.conf > /dev/null
server {
        listen 80;
        listen [::]:80;

        server_name ddnshub.net;
        
        location / {
          proxy_pass http://localhost:8000/;

          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection 'upgrade';
          proxy_set_header x-forwarded-for $proxy_add_x_forwarded_for;
        }
}
EOF

sudo service nginx reload

sudo npm install pm2 -g

# pm2 start /var/www/demo/app.js --name=demo

# Install certbot
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update

sudo apt-get install certbot python3-certbot-nginx -y

sudo certbot --nginx

# emilsolin@hotmail.com # Enter email
# A # Agree on terms and conditions
# N # Share email
# 1 # for ddnhub.net
# 2 # to redirect http traffic to https

# Set environment variables, include all environment variables needed, such as DB_CONNECTION_STRING
cat <<EOF | sudo tee -a /etc/environment > /dev/null
NODE_ENV=production
EOF
# TODO check if environment variables are not read from operatingsystem if dotenv is present


# Setup git repo
sudo mkdir -p /var/repo/site.git && cd /var/repo/site.git
sudo chown $USER /var/repo/site.git
git init --bare

cat > /var/repo/site.git/hooks/post-receive <<'EOF'
#!/bin/sh
APP_NAME="ddnshub"
git --work-tree=/var/www/$APP_NAME --git-dir=/var/repo/site.git checkout -f

cd /var/www/$APP_NAME/web
npm install --production

# The next part is copied from: https://github.com/Unitech/pm2/issues/325#issuecomment-281580956
pm2 delete -s $APP_NAME || :
pm2 start app.js --name=$APP_NAME
EOF

chmod +x /var/repo/site.git/hooks/post-receive
