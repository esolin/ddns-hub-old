'use strict'

const fetch = require('node-fetch')
const { URLSearchParams } = require('url')
const defaultInterval = 1

/**
 * Sets up a "cronjob" for dynamic dns updates with ddnshub.net
 *
 * @param {String} secret - Secret provided by ddnshub.net
 * @param {int} interval - Update interval in minutes. Default 1 minute.
 */
const cronjob = async (secret, interval = defaultInterval) => {
  try {
    throwErrorIfIntervalIsInvalid(interval)
    const url = 'https://ddnshub.net/ip'
    const params = new URLSearchParams()
    params.append('secret', secret)

    await post(url, params)

    setInterval(async () => {
      await post(url, params)
    }, 1000 * 60 * interval)
  } catch (error) {
    console.error(error.message)
  }
}

const throwErrorIfIntervalIsInvalid = (interval) => {
  if (interval < 1) {
    throw new Error(`From ddnshub-cronjob: Interval set to: ${interval}. Should be minimum 1. Could not start cronjob.`)
  }
  if (!Number.isInteger(interval)) {
    throw new Error('From ddnshub-cronjob: The passed argument for interval is not a number. Could not start cronjob.')
  }
}

const post = async (url, params) => {
  try {
    const response = await fetch(url, { method: 'POST', body: params })
    if (response.status !== 200) {
      throw new Error(`Error: ${response.status} ${response.statusText}`)
    }
  } catch (error) {
    console.error(error.message)
  }
}

module.exports = { cronjob }
