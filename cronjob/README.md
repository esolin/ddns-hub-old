This package is used for dynamic dns updates with ddnshub.net


# Installation
```bash
npm install ddnshub-cronjob
```
# Loading package
```javascript
const ddnshub = require('ddnshub-cronjob')
```

# Using package
```javascript
ddnshub.cronjob('secret', updateInterval)
```
Replace 'secret' with the secret for you domain name provided by [ddnshub.net](https://ddnshub.net) and `updateInterval` with the desired update interval in minutes. The default value of `updateInterval` is set to 1 minute if not specified.

### Examples
With specified `updateInterval`
```javascript
ddnshub.cronjob('268c7418c935f435ee41047a9', 5)
```

With default `updateInterval`
```javascript
ddnshub.cronjob('268c7418c935f435ee41047a9')
```