# How to get dynamic DNS updates with this application
1. You need a registered domain name. If you do not have one, register a domain name at a domain name registrar. (For testing purposes, this is not required.)
2. Create an account on [ddnshub.net](https://ddnshub.net) and login.
3. Add domain name under "New Domain"
4. Add the listed name servers to custom name servers at you registrars site.
5. Either include the ddnshub-cronjob package from npm or set up a cronjob on your server/network.


## Include npm package in node  
See [ddnshub-cronjob on npm](https://www.npmjs.com/package/ddnshub-cronjob "npmjs.com/package/ddnshub-cronjob")

## Use cronjob on server  
Set up a cronjob with the desired update interval on the server/network you want dynamic DNS updates for. The cronjob should execute the following command:

```bash
curl -d "secret=your-secret" -X POST https://ddnshub.net/ip
```

Replace "your-secret" with the secret for your domain name provided by [ddnshub.net](https://ddnshub.net).

### Example
```bash
curl -d "secret=268c7418c935f435ee41047a9" -X POST https://ddnshub.net/ip
```