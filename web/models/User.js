'use strict'

const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

// TODO implement validation for invalid email

const usersSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    minlength: 3,
    trim: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  }
}, {
  timestamps: true
})

usersSchema.pre('save', async function () {
  this.password = await bcrypt.hash(this.password, 8)
})

const User = mongoose.model('User', usersSchema)

module.exports = User
