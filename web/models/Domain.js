'use strict'

const mongoose = require('mongoose')

const domainsSchema = new mongoose.Schema({
  domain: {
    type: String,
    unique: true,
    required: true,
    minlength: 4,
    trim: true
  },
  ip: {
    type: String,
    required: false,
    trim: true
  },
  // aRecords: {
  //   type: Object,
  //   required: false
  // },
  // aaaaRecords: {
  //   type: Object,
  //   required: false
  // },
  // nsRecords: {
  //   type: Object,
  //   required: true
  // },
  owner: {
    type: String,
    required: true
  },
  nameServers: {
    type: Array,
    required: true
  },
  secret: {
    type: String,
    required: true
  }
})

const Domain = mongoose.model('Domain', domainsSchema)

module.exports = Domain
