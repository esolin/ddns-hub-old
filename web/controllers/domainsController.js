'use strict'

const flash = require('../lib/flash')
const ns1 = require('../lib/ns1')
const db = require('../lib/db')

const controller = {}

/**
 *
 *
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.Next} next
 */
controller.myDomains = async (req, res, next) => {
  const user = await req.user
  const zones = await db.getDomainsOfUser(user)
  const viewData = { zones: zones }

  res.render('domains/my-domains', { viewData })
}

/**
 *
 *
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.Next} next
 */
controller.newDomain = (req, res, next) => {
  res.render('domains/new-domain')
}

/**
 *
 *
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.Next} next
 */
controller.addDomain = async (req, res, next) => {
  try {
    await ns1.createZone(req)
    await ns1.createDomain(req)
    await ns1.attachNameServersToRequest(req)

    await db.createDomain(req)

    flash.setFlashMessage(req, true, 'Domain added')
    res.redirect('/domains/my-domains')
  } catch (error) {
    flash.setFlashMessage(req, false, error.message)
    res.redirect('/domains/new-domain')
  }
}

controller.editDomain = (req, res, next) => {
  // TODO implementation
}

controller.updateDomain = async (req, res, next) => {
  await ns1.updateZone(req)
  await db.updateDomain(req)
}

controller.removeDomain = (req, res, next) => {
  // TODO implementation

}

controller.deleteDomain = async (req, res, next) => {
  try {
    const domainName = await db.getDomainName(req)
    await ns1.deleteZone(domainName)
    await db.deleteDomain(req.params.id)

    flash.setFlashMessage(req, true, 'Domain deleted')
  } catch (error) {
    flash.setFlashMessage(req, false, error.message)
  }
  res.redirect('/domains/my-domains')
}

module.exports = controller
