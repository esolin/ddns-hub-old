'use strict'

const db = require('../lib/db')
const ns1 = require('../lib/ns1')
const ip = require('../lib/ip')
const controller = {}

controller.index = (req, res, next) => {
  const currentIP = ip.fromRequest(req)
  res.send(JSON.stringify(currentIP))
}

controller.post = async (req, res, next) => {
  try {
    await updateDomainIfIpHasChanged(req)
    res.send('OK')
  } catch (error) {
    // TODO send http error
    console.error(error)
    res.send(error.message)
  }
}

const updateDomainIfIpHasChanged = async (req) => {
  const secret = req.body.secret
  const domain = await db.getDomainFromSecret(secret)

  const newIP = ip.fromRequest(req)
  const oldIP = await ip.fromDomainName(domain)

  if (ip.hasChanged(oldIP, newIP)) {
    const body = setInfo(domain, newIP)
    await ns1.updateDomain({ body })
    await db.updateDomain(domain, newIP)
    // TODO maybe log all changes
  }
}

const setInfo = (domain, ip) => {
  return {
    zone: domain.domain,
    domain: domain.domain,
    ip: ip,
    recordType: 'A'
  }
}

module.exports = controller
