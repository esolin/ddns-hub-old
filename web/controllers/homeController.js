'use strict'

const controller = {}

controller.index = async (req, res, next) => {
  res.render('home/index')
}

controller.about = (req, res, next) => {
  res.render('home/about')
}

controller.contact = (req, res, next) => {
  res.render('home/contact')
}

module.exports = controller
