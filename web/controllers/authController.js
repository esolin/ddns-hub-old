'use strict'

const auth = require('../lib/auth')
const flash = require('../lib/flash')
const db = require('../lib/db')

const controller = {}

controller.index = async (req, res, next) => {
  res.render('auth/index')
}

controller.signUp = async (req, res, next) => {
  res.render('auth/signup')
}

/**
 *
 *
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.Next} next
 */
controller.signUpPost = async (req, res, next) => {
  const email = getEmailFromRequest(req)
  const password = getpasswordFromRequest(req)
  const confirmPassword = getconfirmPasswordFromRequest(req)

  try {
    if (!passwordsMatch(password, confirmPassword)) {
      throw new Error('Passwords does not match')
    }

    await db.createUser(email, password)

    flash.setFlashMessage(req, true, 'Registration successful')
    res.redirect('login')
  } catch (error) {
    if (error.code === 11000) error.message = 'There is already an account registered with that email address'
    flash.setFlashMessage(req, false, error.message)
    res.redirect('signup')
  }
}

controller.login = (req, res, next) => {
  res.render('auth/login')
}

controller.loginPost = async (req, res, next) => {
  auth.passportAuthenticate(req, res, next)
}

controller.logout = (req, res, next) => {
  req.logOut()
  flash.setFlashMessage(req, true, 'Logged out')
  res.redirect('/')
}

const getEmailFromRequest = (req) => req.body.email

const getpasswordFromRequest = (req) => req.body.password

const getconfirmPasswordFromRequest = (req) => req.body.confirmPassword

const passwordsMatch = (password, confirmPassword) => password === confirmPassword

module.exports = controller
