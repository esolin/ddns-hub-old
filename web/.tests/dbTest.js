'use strict'

const db = require('../config/mongoose')
const User = require('../models/User')
const Domain = require('../models/Domain')


const dbTest = () => {
  describe('db', () => {
    before(async () => {
      await setupDB()
    })
  
    after(async () => {
      await dropDB()
      await db.disconnect(true)
    })

    require('./userTest')()
    require('./domainTest')()
  })
}

const setupDB = async () => {
  const dbConnectionString = process.env.TEST_DB_CONNECTION_STRING
  return db.connect(dbConnectionString).catch((err) => {
    console.error(err)
    process.exit(1)
  })
}

const dropDB = async () => {
  await User.collection.drop(() => console.log('User collection dropped'))
  await Domain.collection.drop(() => console.log('Domain collection dropped'))
}

module.exports = dbTest