'use strict'

const Domain = require('../models/Domain')
const db = require('../lib/db')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const domainTest = () => {
  describe('#domain', () => {
    const validDomainName1 = 'considered-valid-domain.com'
    const invalidDomainName = 'considered-invalid-domain'
    const tooShortDomainName = 'a'
    let domain

    describe('createDomain()', () => {
      it('should return an instance of Domain if domain creation succeeds', () => {
        return expect(db.createDomain(prepareRequest(validDomainName1)))
          .to.eventually.be.instanceOf(Domain)
      })

      it('should return true if newly created Domain exists', () => {
        return expect(Domain.exists({ domain: validDomainName1 }))
          .to.eventually.be.true
      })

      it('should throw error if domain already exists', () => {
        return expect(db.createDomain(prepareRequest()))
          .to.eventually.be.rejected
      })

      it('should return domain', async () => {
        domain = await Domain.findOne({ domain: validDomainName1 })
        return expect(domain)
          .to.exist
      })

      // TODO should not be able to create domain with invalid domain name
      // it('should be rejected if domain creation fails', () => {
      //   return expect(db.createDomain(prepareRequest(invalidDomainName)))
      //     .to.eventually.be.rejected
      // })
    })

    describe('deleteDomain()', () => {
      it('should be fulfilled when deleted', () => {
        return expect(db.deleteDomain(domain.id))
          .to.eventually.be.fulfilled
      })

      it('should return false when domain deleted', () => {
        return expect(Domain.exists({ domain: validDomainName1 }))
          .to.eventually.be.false
      })

      it('should be rejected when using non-existing id', () => {
        return expect(db.deleteDomain('non-existing-id'))
          .to.eventually.be.rejected
      })
    })
  })
}

const prepareRequest = (domainName) => {
  return {
    user: { id: '12345' },
    body: {
      domain: domainName,
      nameServers: ['ns1', 'ns2', 'ns3', 'ns4']
    }
  }
}

module.exports = domainTest