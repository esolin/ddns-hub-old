'use strict'

const chai = require('chai')
const should = chai.should()
const chaiAsPromised = require('chai-as-promised')
const User = require('../models/User')
const db = require('../lib/db')
const mongoose = require('../config/mongoose')
const bcrypt = require('bcryptjs')


chai.use(chaiAsPromised)
const assert = chai.assert
const expect = chai.expect

const userTest = () => {
  describe('#user', () => {
    const validEmail1 = 'considered.valid@email'
    const validEmail2 = 'also.considered.valid@email'
    const invalidEmail = 'considered.invalid.email'
    const tooShortEmail = 'a'
    const nonExistentUser = 'non.existent@user'
    const password = 'superSecretPassword'
    const tooShortPassword = 'short'
    let user

    describe('createUser()', () => {
      it('should return an instance of User if user creation succeeds', async () => {
        return expect(db.createUser(validEmail1, password))
          .to.eventually.be.instanceOf(User)
      })

      it('should return true if newly created user exists', () => {
        return expect(User.exists({ email: validEmail1 }))
          .to.eventually.be.true
      })

      it('should throw error if username is taken', () => {
        return expect(db.createUser(validEmail1, password))
          .to.eventually.be.rejected
      })

      it('should return user', async () => {
        user = await User.findOne({ email: validEmail1 })
        return expect(user)
          .to.exist
      })

      it('should return false if user is non-existent', () => {
        return expect(User.exists({ email: nonExistentUser }))
          .to.eventually.be.false
      })

      it(`should return User with property email with value ${validEmail1}`, async () => {
        return expect(User.findOne({ email: validEmail1 }))
          .to.eventually.have.property('email', validEmail1)
      })

      // TODO test if user is deleted

      // TODO implement validation for invalid email in User.js
      // it(''should throw error if email is invalid', async () => {
      //   return expect(await User.createUser(invalidEmail, password))
          //  .to.eventually.be.rejected
      // })

      it('should throw error if email is too short', async () => {
        return expect(db.createUser(tooShortEmail, password))
          .to.eventually.be.rejected
      })

    })
    
    describe('#password', () => {
      it('should return true if password is hashed', async () => {
        return expect(bcrypt.compare(password, user.password))
          .to.eventually.be.true
      })

      it('should throw error if password is too short', () => {
        return expect(db.createUser(validEmail1, tooShortPassword))
          .to.eventually.be.rejected
      })
    })

    describe('deleteUser()', () => {
      it('should pass if user deletion succeeds', async () => {
        return expect(db.deleteUser(validEmail1, password))
          .to.eventually.equal('User deleted')
      })

      // TODO throw erro when deleting non-existing user ?????????
      // it('should throw error when delete non-existent User', async () => {
      //   return expect(User.deleteUser(validEmail1, password))
      //     .to.eventually.equal('User deleted')
      // })

      it('should pass if deleted user does not exist', async () => {
        return expect(User.exists({ email: validEmail1}))
          .to.eventually.be.false
      })
    })
  })
}

module.exports = userTest