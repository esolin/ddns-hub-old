'use strict'

const assert = require('assert')

const arrayTest = () => { 
  describe('Array', () => {
    describe('#indexOf()', () => {
      it('should return -1 when the value is not present', () => {
        assert.equal([1, 2, 3].indexOf(4), -1)
      })
    })
    describe('#isArray()', () => {
      it('should return true if instance of Array', () => {
        assert.ok([1, 2, 3] instanceof Array)
      })
    })
    describe('#isNotArray()', () => {
      it('should return true if not instance of Array', () => {
        assert.ok(!('[1, 2, 3]' instanceof Array))
      })
    })
  })
}

module.exports = arrayTest