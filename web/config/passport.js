const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcryptjs')
const User = require('../models/User')

/**
 *
 *
 * @param {passport} passport
 */
const initialize = (passport) => {
  const authenticateUser = async (email, password, done) => {
    const user = await getUserByEmail(email)
    const invalidLoginCredentials = 'Invalid login credentials'
    const failureFlash = { type: 'failure', message: invalidLoginCredentials }

    if (user == null) {
      return done(null, false, failureFlash)
    }

    const successFlash = { type: 'success', message: `Welcome ${user.email}` }

    try {
      if (await bcrypt.compare(password, user.password)) {
        return done(null, user, successFlash)
      } else {
        return done(null, false, failureFlash)
      }
    } catch (error) {
      return done(error)
    }
  }

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))

  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => done(null, getUserById(id)))
}

const getUserByEmail = async (email) => {
  const user = await User.findOne({ email: email })
  return user
}

const getUserById = async (id) => {
  const user = await User.findById(id)
  return user
}

module.exports = initialize
