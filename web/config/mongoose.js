'use strict'

const mongoose = require('mongoose')

const connect = async (connectionString = process.env.DB_CONNECTION_STRING) => {
  mongoose.connection.on('connected', () => console.log('Mongoose connection is open.'))
  mongoose.connection.on('error', (err) => console.error(`Mongoose connection error has occured: ${err}`))
  mongoose.connection.on('disconnected', () => console.log('Mongoose connection is disconnected.'))

  process.on('SIGINT', () => {
    disconnect()
    // mongoose.connection.close(() => {
    //   console.log('Mongoose connection is disconnected due to application termination.')
    //   process.exit(0)
    // })
  })

  return mongoose.connect(connectionString, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
    useFindAndModify: false
  })
}

const disconnect = (testEnvironment) => {
  mongoose.connection.close(() => {
    console.log('Mongoose connection is disconnected due to application termination.')
    if (!testEnvironment) process.exit(0)
  })
}

module.exports = { connect, disconnect }
