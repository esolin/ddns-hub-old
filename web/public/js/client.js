const copyToClipboard = () => {
  const nodes = document.querySelectorAll('.copy')

  nodes.forEach(node => {
    node.addEventListener('click', function (event) {
      const textArea = document.createElement('textarea')
      textArea.value = this.innerText
      this.appendChild(textArea)

      textArea.focus()
      textArea.select()
      document.execCommand('copy')

      this.removeChild(textArea)
    })
  })
}

const passwordLengthColor = () => {
  try {
    const password = document.querySelector('#newPassword')

    if (!password) return

    password.addEventListener('keydown', function (event) {
      if (this.value.length < 6) {
        this.style.borderColor = '#CC0000'
      }
      switch (this.value.length) {
        case 5: this.style.borderColor = '#FF0000'
          break
        case 7: this.style.borderColor = '#FF8000'
          break
        case 9: this.style.borderColor = '#FFFF00'
          break
        case 11: this.style.borderColor = '#BFFF00'
          break
        case 13: this.style.borderColor = '#00FF00'
          break
        case 15: this.style.borderColor = '#00D100'
          break
      }
    })
  } catch (error) {
    console.error(error)
  }
}

copyToClipboard()
passwordLengthColor()
