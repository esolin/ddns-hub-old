'use strict'

// TODO Delete if not used. I believe everything good as been implemented in the cronjob package.

const fetch = require('node-fetch')
const { URLSearchParams } = require('url')

const postIP = async (secret, interval) => {
  const url = getUrl()
  const params = new URLSearchParams()
  params.append('secret', secret)
  await fetch(url, { method: 'POST', body: params })
    .then(res => res.json)
    .then(json => console.log(json))
}

const getUrl = (secret) => {
  return `https://ddnshub.net/ip`
}

const monitorIP = async () => {
  let currentIP = await fetchIP()
  let newIP
  await fetch('http://checkip:5000')
  console.log(currentIP)
  setInterval(async () => {
    // newIP = await fetchIP()
    // const test = await fetch('http://checkip:5000')

    // if (newIP !== currentIP) {
    //   currentIP = newIP
    //   console.log(currentIP)
    // }
  }, 1000 * 30)
}

const fetchIP = async () => {
  try {
    const ip = await fetch('https://ifconfig.me/all.json')
      .then(response => response.json())
      .then(data => data.ip_addr)

    return ip
  } catch (error) {
    console.error(error)
  }
}
module.exports = { postIP }
