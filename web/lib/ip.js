'use strict'

const dig = require('node-dig-dns')

const fromRequest = (request) => {
  return request.headers['x-forwarded-for'] || request.connection.remoteAddress
}

// TODO Is this still used?
const fromDomainName = (domain) => {
  return dig([`@${domain.nameServers[0]}`, domain.domain, '+short'])
}

const hasChanged = (oldIP, newIP) => {
  return oldIP !== newIP
}

module.exports = {
  fromDomainName,
  fromRequest,
  hasChanged
}
