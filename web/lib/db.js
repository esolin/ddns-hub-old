'use strict'

const User = require('../models/User')
const Domain = require('../models/Domain')
const crypto = require('crypto')

const createUser = async function (email, password) {
  const user = new User({
    email: email,
    password: password
  })

  // TODO return user.save() ?
  await user.save()
  return user
}

const deleteUser = async function (email, password) {
  // TODO return the following
  await User.findOneAndDelete({ email: email })
  // User.findOneAndRemove

  return 'User deleted'
}

const createDomain = async (req) => {
  // TODO should not be able to create domain with invalid domain name
  const user = await req.user
  const domain = new Domain({
    domain: req.body.domain,
    owner: user.id,
    nameServers: req.body.nameServers,
    secret: createToken()
  })

  // TODO return domain.save() ?
  await domain.save()
  return domain
}

const deleteDomain = (id) => {
  return Domain.findByIdAndDelete(id)
}

const updateDomain = async (domain, newIP) => {
  // TODO return the followin?
  await Domain.findByIdAndUpdate(domain.id, { ip: newIP })
}

const getDomainsOfUser = async (user) => {
  // TODO break this out to domain.js as static function
  return (await Domain.find({ owner: user.id }))
    .map(domain => ({
      domain: domain.domain,
      user: user.email,
      secret: domain.secret,
      id: domain.id,
      currentIP: domain.ip,
      nameServers: domain.nameServers
    }))
}

const getDomainName = async (req) => {
  // TODO take id as param, not req
  const domain = await Domain.findById(req.params.id)
  return domain.domain
}

const getDomainFromSecret = async (secret) => {
  let domain

  try {
    domain = await Domain.findOne({ secret: secret })
  } catch (error) {
    console.error(error)
  }
  throwErrorIfdomainNotExist(domain)

  return domain
}

const throwErrorIfdomainNotExist = (domain) => {
  if (!domain) {
    throw new Error('The secret is not valid')
  }
}

const createToken = () => {
  // TODO check if secure, should be hashed before stored in db.
  const size = 25
  return crypto.randomBytes(size).toString('hex').slice(0, size)
}

module.exports = {
  createUser,
  deleteUser,
  createDomain,
  deleteDomain,
  updateDomain,
  getDomainsOfUser,
  getDomainName,
  getDomainFromSecret
}
