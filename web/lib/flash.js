'use strict'

const setFlashMessage = (req, type, message) => {
  req.session.flash = {
    type: type ? 'success' : 'failure',
    message: message
  }
}

module.exports = { setFlashMessage }
