'use strict'

const passport = require('passport')

// To only allow authenticated users
const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next()
  }

  res.redirect('/auth/login')
}

// To only allow access to non authenticated users, like login page
const isNotAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    // TODO maybe set a flash message
    return res.redirect('/')
  }

  next()
}

const passportAuthenticate = async (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err)
    }

    req.session.flash = info

    if (!user) {
      return res.redirect('/auth/login')
    }

    req.logIn(user, (err) => {
      if (err) {
        return next(err)
      }

      return res.redirect('/domains/my-domains')
    })
  })(req, res, next)
}

module.exports = { isAuthenticated, isNotAuthenticated, passportAuthenticate }
