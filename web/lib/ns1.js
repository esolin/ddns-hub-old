'use strict'
const fetch = require('node-fetch')

const createZone = async (req) => {
  const form = req.body
  const url = getUrl(form.domain)

  const headers = setHeaders()
  const body = getZoneFromForm(form)
  const options = setOptions('PUT', headers, body)

  await sendRequest(url, options)
  // await saveRecord(`${url}/${form.zone}`, options, 'A')
}

const updateZone = async (req) => {
  const form = req.body
  const url = getUrl(form.domain)

  const headers = setHeaders()
  const body = getZoneFromForm(form)
  const options = setOptions('POST', headers, body)

  await sendRequest(url, options)
}

const deleteZone = async (zone) => {
  const url = getUrl(zone)
  const headers = setHeaders()
  const options = setOptions('DELETE', headers)

  await sendRequest(url, options)
}

const createDomain = async (req) => {
  const form = req.body
  const zone = form.domain || form.zone
  const domain = form.domain || form.zone
  const recordType = form.recordType || 'A'

  const url = getUrl(zone, domain, recordType)

  const headers = setHeaders()
  const body = getRecordFromForm(form)
  const options = setOptions('PUT', headers, body)

  await sendRequest(url, options)
}

const updateDomain = async (req) => {
  const form = req.body
  const zone = form.zone
  const domain = form.domain || form.zone
  const recordType = form.recordType || 'A'

  const url = getUrl(zone, domain, recordType)

  const headers = setHeaders()
  const body = getRecordFromForm(form)
  const options = setOptions('POST', headers, body)

  await sendRequest(url, options)
}

const deleteDomain = (req) => {

}

const getUrl = (zone, domain, recordType) => {
  const baseUrl = 'https://api.nsone.net/v1/zones'

  const url = recordType
    ? `${baseUrl}/${zone}/${domain}/${recordType}`
    : `${baseUrl}/${zone}`

  return url
}

const setHeaders = () => {
  return {
    'X-NSONE-Key': process.env.NS1_API_KEY,
    'Content-Type': 'application/json'
  }
}

const getZoneFromForm = (form) => {
  const ttl = form.ttl || 60

  // The return value should be compatible with the ns1 rest api
  return {
    zone: form.domain,
    ttl: ttl, // TODO This should be set automatically to a standard, say 300, or from the request form
    nx_ttl: ttl
  }
}

const getRecordFromForm = (form) => {
  const ttl = form.ttl || 60
  const zone = form.zone || form.domain
  const domain = form.domain
  const recordType = form.recordType || 'A'
  const ipAddress = form.ip || '1.3.5.2'

  // The return value should be compatible with the ns1 rest api
  return {
    zone: zone,
    domain: domain,
    type: recordType,
    answers: [{ answer: [ipAddress] }],
    ttl: ttl
  }
}

const setOptions = (method, headers, body) => {
  return {
    method: method,
    headers: headers,
    body: JSON.stringify(body)
  }
}

const sendRequest = async (url, options) => {
  const res = await fetch(url, options)
    .then(async (res) => ({
      status: res.status,
      body: await res.json()
    }))
  // TODO Handle error like this: https://www.npmjs.com/package/node-fetch#handling-client-and-server-errors
  throwErrorIfNotOK(res)
}

const attachNameServersToRequest = async (req) => {
  const zone = req.body.domain
  const url = getUrl(zone)
  const headers = setHeaders()
  const options = setOptions('GET', headers)

  const response = await fetch(url, options)
    .then(async (res) => ({
      status: res.status,
      body: await res.json()
    }))
  throwErrorIfNotOK(response)

  req.body.nameServers = response.body.dns_servers
}

const throwErrorIfNotOK = (res) => {
  if (res.status !== 200) {
    throw new Error(res.body.message)
  }
}

module.exports = {
  createZone,
  deleteZone,
  updateZone,
  createDomain,
  updateDomain,
  deleteDomain,
  attachNameServersToRequest
}
