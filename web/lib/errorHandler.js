'use strict'

const path = require('path')
const createError = require('http-errors')

// TODO implement script that checks which error pages exists in views/errors.
/**
 *
 *
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const errorHandler = (err, req, res, next) => {
  const errorCode = err.statusCode
  const errorHTML = errorHTMLExists(errorCode)
    ? getErrorHTMLPage(errorCode)
    : getInternalServerErrorHTMLPage()

  return res
    .status(errorCode)
    .sendFile(errorHTML)
}

/**
 *
 *
 * @param {*} errCode
 * @returns
 */
const errorHTMLExists = (errCode) => {
  // TODO include codes from filenames error dir
  const errorCodesToHandle = [401, 403, 404]
  return errorCodesToHandle.includes(errCode)
}

/**
 *
 *
 * @param {*} errCode
 * @returns
 */
const getErrorHTMLPage = (errCode) => {
  const errorsDir = getErrorsPath()
  return path.join(errorsDir, `${errCode}.html`)
}

/**
 *
 *
 * @returns
 */
const getInternalServerErrorHTMLPage = () => {
  const errorsDir = getErrorsPath()
  return path.join(errorsDir, '500.html')
}

/**
 *
 *
 * @returns
 */
const getErrorsPath = () => {
  return path.join(__dirname, '..', 'views', 'errors')
}

/**
 *
 *
 * @param {*} errorCode
 * @returns
 */
const newError = (errorCode) => {
  return (req, res, next) => {
    next(createError(errorCode))
  }
}

module.exports = { errorHandler, newError }
