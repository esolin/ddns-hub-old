'use strict'

if (process.env.NODE_ENV !== 'production') require('dotenv').config()
const express = require('express')
const hbs = require('express-hbs')
const path = require('path')
const cookieParser = require('cookie-parser')
const sessionConfig = require('./config/session')
const passport = require('passport')
const initializePassport = require('./config/passport')

const { errorHandler, newError } = require('./lib/errorHandler')
const mongoose = require('./config/mongoose')

const homeRouter = require('./routes/homeRouter')
const authRouter = require('./routes/authRouter')
const domainsRouter = require('./routes/domainsRouter')
const ipRouter = require('./routes/ipRouter')

const app = express()
const port = process.env.PORT || 8000

initializePassport(passport)

mongoose.connect().catch((err) => {
  console.error(err)
  process.exit(1)
})

const viewsPath = path.join(__dirname, 'views')

app.engine('hbs', hbs.express4({
  defaultLayout: path.join(viewsPath, 'layouts', 'default'),
  partialsDir: path.join(viewsPath, 'partials')
}))
app.set('view engine', 'hbs')
app.set('views', viewsPath)

// Middleware
// TODO includeDevMiddleware(app)
if (process.env.NODE_ENV !== 'production') {
  app.use(require('morgan')('dev'))
}
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(sessionConfig.options())
app.use(sessionConfig.flash)

app.use(passport.initialize())
app.use(passport.session())

// Routes
app.use('/', homeRouter)
app.use('/auth', authRouter)
app.use('/domains', domainsRouter)
app.use('/ip', ipRouter)

// Error handling
app.use('*', (newError(404)))
app.use(errorHandler)

app.listen(port, () => console.log(`Server listens to http://localhost:${port}`))
