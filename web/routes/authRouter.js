'use strict'

const express = require('express')
const controller = require('../controllers/authController')
const auth = require('../lib/auth')

const router = express.Router()

router.get('/', controller.index)

router.get('/signup', auth.isNotAuthenticated, controller.signUp)
router.post('/signup', auth.isNotAuthenticated, controller.signUpPost)

router.get('/login', auth.isNotAuthenticated, controller.login)
router.post('/login', auth.isNotAuthenticated, controller.loginPost)

router.get('/logout', controller.logout)

module.exports = router
