'use strict'

const express = require('express')
const controller = require('../controllers/domainsController')
const auth = require('../lib/auth')
const router = express.Router()

router.get('/my-domains', auth.isAuthenticated, controller.myDomains)

router.get('/new-domain', auth.isAuthenticated, controller.newDomain)
router.post('/add-domain', auth.isAuthenticated, controller.addDomain)

router.get('/:id/edit-domain', auth.isAuthenticated, controller.editDomain)
router.post('/:id/update-domain', auth.isAuthenticated, controller.updateDomain)

router.get('/:id/remove-domain', auth.isAuthenticated, controller.removeDomain)
router.post('/:id/delete-domain', auth.isAuthenticated, controller.deleteDomain)

module.exports = router
