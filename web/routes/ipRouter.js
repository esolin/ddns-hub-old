'use strict'

const express = require('express')
const controller = require('../controllers/ipController')

const router = express.Router()

router.get('/', controller.index)
router.post('/', controller.post)

module.exports = router
